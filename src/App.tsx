import React from 'react';
import './App.css';
import ImageUpload from './components/image-upload/image-upload';

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <ImageUpload></ImageUpload>
        <p>
          slu<span className="emphasize-gif">gif</span>y
        </p>
      </header>
    </div>
  );
}

export default App;
