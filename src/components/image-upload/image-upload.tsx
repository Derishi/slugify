import React from 'react';
import {fabric} from 'fabric';
import head from '../../resources/alex_head.png';
import './image-upload.css';
import { Image, StaticCanvas } from 'fabric/fabric-impl';
import GIFEncoder from '../../local_dependency/GIFEncoder';
import { encode64 } from '../../local_dependency/b64';

type MyState = {
    gifArray: any[];
}

export default class ImageUpload extends React.Component<{}, MyState> {
    private NUMBER_OF_SEGMENTS = 50;
    private NUMBER_OF_PANELS = 2;
    private FRAME_DELAY = 30;
    private LEFT_SHIFT = 20;
    private SPEED = 1.0;
    private DIMENSION = 128;
    public canvas: StaticCanvas;
    public imageElement: HTMLImageElement;
    public gifElement: HTMLImageElement;
    private middlePoint: number;
    private lastPoint: number;
    private frameLength: number = 0;
    private encoderArray: any[] = [];
    private encoder: any;
    private encoderStatus: string;
    private fs: any;
    private buffer: any;
    private gifArray: any[] = [];

    constructor(props:any) {
        super(props);

        this.state = {
            gifArray: []
        }

        // This binding is necessary to make `this` work in the callback
        this.renderCanvas = this.renderCanvas.bind(this);
    }

    componentDidMount() {
        //Initialize Canvas Dependencies
        this.canvas = new fabric.StaticCanvas('upload-preview');
        this.canvas.setHeight(this.DIMENSION);
        this.canvas.setWidth(this.DIMENSION * this.NUMBER_OF_PANELS);
        this.imageElement = document.getElementById('image') as HTMLImageElement;
        this.gifElement = document.getElementById('my-gif') as HTMLImageElement;

        this.middlePoint = (this.canvas.getHeight() / 2);
        this.lastPoint = this.middlePoint;

        this.encoder = new GIFEncoder();
        this.encoderStatus = "IDLE";

        this.canvas.on('after:render', (event) => {
            this.addFrame();
        });

        setInterval(()=>{
            this.animateSlug();
        }, this.FRAME_DELAY);
    }

    public animateSlug() {
        this.renderCanvas((new Date().getTime())/20);
    }

    public renderCanvas:any = (timeOffset:number = 0) => {
        this.canvas.clear();

        let imageInstance = new fabric.Image(this.imageElement, {
            centeredScaling: true,
            originX: "center",
            originY: "center"
        });
        imageInstance.scaleToHeight(this.DIMENSION);

        let leftOffset = (imageInstance.getScaledWidth() / 2) - this.LEFT_SHIFT;
        for (let i = 0; i <= Math.floor(this.NUMBER_OF_SEGMENTS * 0.9); i++) {
            // eslint-disable-next-line
            imageInstance.cloneAsImage((clone: Image)=>{
                clone.originX = "center";
                clone.originY = "center;"
                let scale = (this.NUMBER_OF_SEGMENTS - i) / this.NUMBER_OF_SEGMENTS;
                clone.scale(scale);

                //Wave function
                let yWaveFunc = (Math.sin(this.SPEED * (2*Math.PI*(i+timeOffset)/this.NUMBER_OF_SEGMENTS)));
                //let xWaveFunc = (Math.sin(0.5 * (2*Math.PI*(i+timeOffset)/this.NUMBER_OF_SEGMENTS)));

                //Horizontal Wiggle
                clone.left = leftOffset + (((this.DIMENSION * this.NUMBER_OF_PANELS) - (2*this.LEFT_SHIFT)) / this.NUMBER_OF_SEGMENTS);
                leftOffset = clone.left;

                //Vertical Wiggle
                clone.top = (this.canvas.getHeight() / 2) + -(yWaveFunc * (((i+(this.NUMBER_OF_SEGMENTS/10))/this.NUMBER_OF_SEGMENTS)*(clone.height/2)*scale));
                clone.setCoords();
                this.canvas.add(clone);
                this.canvas.moveTo(clone, -i);

                if (i === 0) {
                    this.frameLength++;
                    //Find middle point
                    if (this.lastPoint <= this.middlePoint && this.middlePoint <= clone.top) { 
                        console.log("Looped! Current Frame Length is:" + this.frameLength);
                        this.frameLength = 0;

                        if (this.encoderStatus === "PROCESSING") {
                            this.encoderStatus = "PROCESSING_FINAL";
                        }
                        else if (this.encoderStatus === "PREPROCESSING") {
                            console.log("Encoding Started");
                            this.encoderStatus = "PROCESSING";
                        }
                    }
                    this.lastPoint = clone.top;
                }
            });
        }

        this.canvas.backgroundColor = "white";

        if (this.encoderStatus === "IDLE") {
            this.drawSegmentLines();
        }
    }

    private drawSegmentLines: any = () => {
        for(let i = 1; i < this.NUMBER_OF_PANELS; i++) {
            let segmentX = (this.canvas.getWidth() / this.NUMBER_OF_PANELS) * i;
            let segmentBorder = new fabric.Line([segmentX, 0, segmentX, this.canvas.getHeight()], {
                fill: 'black',
                stroke: 'black',
                strokeWidth: 1
            });
            this.canvas.add(segmentBorder);
        }
    }

    private intializeEncoder = () => {
        for (let i = 0; i < this.NUMBER_OF_PANELS; i++) {
            this.encoderArray[i] = new GIFEncoder();
            this.encoderArray[i].start();
            this.encoderArray[i].setRepeat(0);
            this.encoderArray[i].setDelay(this.FRAME_DELAY);
            this.encoderArray[i].setQuality(10);
        }
    }

    private createGif: any = () => {
        if (this.encoderStatus === "IDLE") {
            console.log("Starting Encode");
            this.encoderStatus = "PREPROCESSING";
            this.intializeEncoder();
        }
        else {
            console.log("Encoding already in progress");
        }
        
    }

    private addFrame = () => {
        if(this.encoderStatus === "PROCESSING") {
            //Need to split the canvas somehow
            let ctxArray = this.getContexts();
            for(let i = 0; i < this.NUMBER_OF_PANELS; i++) {
                this.encoderArray[i].addFrame(ctxArray[i]);
            }
        }

        if(this.encoderStatus === "PROCESSING_FINAL") {
            let ctxArray = this.getContexts();
            for(let i = 0; i < this.NUMBER_OF_PANELS; i++) {
                this.encoderArray[i].addFrame(ctxArray[i]);
                this.encoderArray[i].finish();

                let gif_data = this.encoderArray[i].stream().getData();
                this.gifArray[i] = "data:image/gif;base64," + encode64(gif_data);
            }

            this.setState({
                gifArray: this.gifArray
            });

            console.log("Encode Complete");
            this.encoderStatus = ("IDLE");
        }
    }

    /**
     * Helper method to split canvas into smaller contexts to render individual GIFs
     */
    private getContexts = () => {
        let canvasArray = [];
        let segmentWidth = (this.canvas.getWidth() / this.NUMBER_OF_PANELS);
        for (let i = 0; i < this.NUMBER_OF_PANELS; i++) {
            canvasArray.push(this.cropCanvas(this.canvas, (segmentWidth * i), 0, segmentWidth, this.DIMENSION));
        }
        return canvasArray.map((canvas) => {
            return canvas.getContext("2d");
        });
    }

    /**
     * Crops a Canvas into a Smaller Canvas
     */
    private cropCanvas = (sourceCanvas: StaticCanvas, left: number, top: number, width: number, height: number) => {
        let destCanvas = document.createElement('canvas');
        destCanvas.width = width;
        destCanvas.height = height;
        destCanvas.getContext("2d").drawImage(
            sourceCanvas.getElement(),
            left,top,width,height,  // source rect with content to crop
            0,0,width,height);      // newCanvas, same size as source rect
        return destCanvas;
    }
    
    public render() {
        const images = this.state.gifArray.map((data, index) => {
            return <img className="gif-segment" key={index} src={data} alt=""></img>;
        })
        
        return(
            <div>
                <button className="upload-button" onClick={this.renderCanvas}>Upload</button>
                <button className="create-gif" onClick={this.createGif}>Create GIF</button>

                <img id="image" src={head} className="hide" alt=""></img>
                <div>
                    <canvas id="upload-preview"></canvas>
                </div>
                <div>
                    <header>
                        You're GIF will Appear Below
                    </header>
                    <div>
                        {images}
                    </div>
                </div>
            </div>
        );
    }

}