/**
 * Helper class to hold ByteArray information
 * Base class :  http://www.java2s.com/Code/Java/2D-Graphics-GUI/AnimatedGifEncoder.htm
 * @author Kevin Weiner (original Java version - kweiner@fmsware.com)
 * @author Thibault Imbert (AS3 version - bytearray.org)
 * @author Kevin Kwok (JavaScript version - https://github.com/antimatter15/jsgif)
 * @author Alex Villasenor (TypeScript version)
 * @version 0.1 AS3 implementation
 */

export default class ByteArray {
	private bin: any;
	private chr: any = {};

	constructor() {
		this.bin = [];

		for (let i = 0; i < 256; i++) {
			this.chr[i] = String.fromCharCode(i);
		}
	}

	public getData = () => {
		for (var v = '', l = this.bin.length, i = 0; i < l; i++)
			v += this.chr[this.bin[i]];
		return v;
	}

	public writeByte = (val: any) => {
		this.bin.push(val);
	}

	public writeUTFBytes = (string: string) => {
		for (var l = string.length, i = 0; i < l; i++)
			this.writeByte(string.charCodeAt(i));
	}

	public writeBytes = (array: any, offset :any, length :any) => {
		for (var l = length || array.length, i = offset || 0; i < l; i++)
		this.writeByte(array[i]);
	}
}